using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    protected float hp;
    protected float moveSpeed;
    protected float shootSpeed;
    protected Vector2 shootDirection;
    protected float damageMelee;
    protected float damageRange;
    protected Rigidbody2D rigidBody2D;
    
  [SerializeField]  protected List<GameObject> weapons; 

    protected virtual void rotateSprite()
    {

    }
    protected virtual void rangeAtack()
    {

    }

    protected virtual void meleeAtack(){
        
    }

    protected virtual void moveCharacter()
    {

    }
    protected virtual void changeSpeed(float changeSpeed)
    {

    }
    protected virtual void changeLife()
    {

    }
}
