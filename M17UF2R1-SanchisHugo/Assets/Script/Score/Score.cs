using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[System.Serializable]
public class Score {
    public List<float> scores;
    public int wins;
   public Score() { 
    
        scores = new List<float>();
        wins = 0;
    }
}

