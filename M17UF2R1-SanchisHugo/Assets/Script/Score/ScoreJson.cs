using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEditor.Progress;

public class ScoreJson : MonoBehaviour
{

    void Start()
    {
        if (File.Exists("scoreSR.txt")) jsonFileExists("scoreSR.txt") ;
        else jsonFileNotExists("scoreSR.txt");
    }
    void jsonFileNotExists(string filename)
    {

        var score = new Score();
       
         float transformscore = Convert.ToInt32(PlayerPrefs.GetString("scoreNum"));
        score.scores.Add(transformscore);
        if (PlayerPrefs.GetString("scoreText") == "You Win") score.wins = 1;
        else score.wins = 0;
        
      // string a = JsonSerializer.Serialize(score,true);
      
        string json = JsonUtility.ToJson(score);
        File.WriteAllText(filename, json);
    }
    void jsonFileExists(string filename)
    {
        string scoretext = File.ReadAllText(filename);
        var score = JsonUtility.FromJson<Score>(scoretext);
        var lastscore = Convert.ToInt32(PlayerPrefs.GetString("scoreNum"));
        if (score.scores.Count >= 10) { if (score.scores[9] < lastscore) score.scores[9] = lastscore; }
        else score.scores.Add(lastscore);
        orderScoreList(score);

        if (PlayerPrefs.GetString("scoreText") == "You Win") score.wins += 1;
        else score.wins = 0;
        
        File.WriteAllText(filename, JsonUtility.ToJson(score));
    }

    void orderScoreList(Score score)
    {
      
         score.scores.Sort((a, b) => b.CompareTo(a));

    }


  
}
