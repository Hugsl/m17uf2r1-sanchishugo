using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Enemy_Controler : BaseEnemy
{

    private float cooldown;

    // Start is called before the first frame update
    void Start()
    {
        
        cooldown = enemy_data.coolDownRange;
        hp = enemy_data.hp;
        moveSpeed = enemy_data.moveSpeed;
        damageRange = enemy_data.damage;
       
       
    }

    // Update is called once per frame
    void Update()
    {
        MC_Data = GameObject.Find("Player").GetComponent<MC_Controler>().MC_Data;
        transform.LookAt(Player);
        Quaternion rotation = Quaternion.LookRotation(Player.transform.position - transform.position, transform.TransformDirection(Vector3.up));
        if (Vector3.Distance(transform.position, Player.position) >= enemy_data.minRange+1)
        {
            transform.position += (transform.forward) * moveSpeed * Time.deltaTime;
            if (cooldown > 0)
            {
                cooldown -= Time.deltaTime;
            }
            else {

                Vector2 pos = new Vector2(transform.position.x, transform.position.y);
                GameObject objectToSpawn = weapons[0];
                Instantiate(objectToSpawn, pos, transform.rotation);
                cooldown = enemy_data.coolDownRange;
            }
           
        }
        else if (Vector3.Distance(transform.position, Player.position) <= enemy_data.maxRange)
        {
            Player.position = new Vector3(Player.position.x, Player.position.y, Player.position.z);
            transform.position += (transform.forward*-0.5f) * moveSpeed * Time.deltaTime;
        }
        transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
        
    }


    
    public override void reciveDamage(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            base.reciveDamage(hp);
            var a = GameObject.Find("Canvas").GetComponent<UI_Controler>();
            a.setPunctuation(enemy_data.punctuation);
            Destroy(this.gameObject);
        }
    }
}
