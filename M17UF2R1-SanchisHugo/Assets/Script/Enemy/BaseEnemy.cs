using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : Character
{
  
    [SerializeField] protected MC MC_Data;
    [SerializeField] protected Enemy enemy_data;
   

    protected Transform Player;


    public void OnEnable()
    {
        Enemy.SpawnEnemies -= spawn;
        Enemy.SpawnEnemies += spawn;
    }
    protected void Awake()
    {
        Player = GameObject.Find("Player(clone)").GetComponent<Transform>();
        var a = GameObject.Find("Player(clone)").GetComponent<MC_Controler>();
        MC_Data = a.MC_Data;
    }

    protected virtual void spawn()
    {

    }
    public virtual void reciveDamage(float damage)
    {
        var b = this.GetComponent<AudioSource>();
        b.Play();
        if (hp<= 0)
        {
            var a = GetComponent<Collider2D>();
            a.enabled = false;
        }
    }
}
