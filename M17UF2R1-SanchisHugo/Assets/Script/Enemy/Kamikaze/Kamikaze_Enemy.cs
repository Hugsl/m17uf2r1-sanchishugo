using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamikaze_Enemy : BaseEnemy, IDropeable
{
    
    
    void Start()
    { 
        
        hp = enemy_data.hp;
        moveSpeed = enemy_data.moveSpeed;
        damageMelee = enemy_data.damage;
       

    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Player);
        Quaternion rotation = Quaternion.LookRotation(Player.transform.position - transform.position, transform.TransformDirection(Vector3.up));

        if (Vector2.Distance(transform.position, Player.position) >= enemy_data.minRange)
        {
           
            transform.position += transform.forward * moveSpeed * Time.deltaTime;



            if (Vector3.Distance(transform.position, Player.position) <= enemy_data.maxRange)
            {
                //Here Call any function U want Like Shoot at here or something
            }
        }
        transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
    }



   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            MC_Data.SetHealth(damageMelee*-1);
            
            Destroy(this.gameObject);
        }
    }

    void prepareDrop()
    {
        GameObject Item = enemy_data.dropeableItems[0];
        dropItem(Item);
    }
    void dropItem(GameObject item)
    {
        Instantiate(item, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
    }
    public override void reciveDamage(float damage)
    {
        base.reciveDamage(damage);
        hp -= damage;
        if (hp <= 0)
        {
            base.reciveDamage(hp);
            prepareDrop();
            var a = GameObject.Find("Canvas").GetComponent<UI_Controler>();
            a.setPunctuation(enemy_data.punctuation);
            Destroy(this.gameObject);
        }
    }
}
