using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Enemy_X_Data", menuName = "New Asset/New Enemy")]

public class Enemy : ScriptableObject
{
    public static event Action SpawnEnemies = delegate { };
    public float hp;
    public float moveSpeed;
    public float shootSpeed;
    public float damage;
    public float minRange;
    public float maxRange;
    public float coolDownRange;
    public List<GameObject> dropeableItems;
    public int punctuation;

    public void TriggerEvent()
    {
        SpawnEnemies.Invoke();
    }
    
}
