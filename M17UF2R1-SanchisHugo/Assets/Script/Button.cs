using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
   public void OnGame()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void OnStart()
    {
        SceneManager.LoadScene("StartScene");
    }
    public void OnExit()
    {
        Application.Quit();
    }
}
