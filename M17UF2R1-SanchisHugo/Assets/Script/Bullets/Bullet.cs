using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bullet_Data", menuName = "New Asset/New Bullet")]
public class Bullet : ScriptableObject
{
    public float Bullet_Speed;
    public float Bullet_Damage;
    public float Bullet_Max;
    public float Charge;
    [SerializeField] private float _Bullet_Left;
    [SerializeField] private float _Bullet_LeftInCharger;
    public float Bullet_ChargerCapacity;
    public float Bullet_ReloadTime;
    public float Bullet_TimeBetweenBulets;


    private void OnAwake()
    {
        _Bullet_Left = 0;
        _Bullet_LeftInCharger = 0;
        SetBulletLeftInCharger(Charge);
        SetBulletLeft(Bullet_Max);

    }

    public void SetBulletLeft(float bullets)
    {
        _Bullet_Left += bullets;
    }
    public float GetBulletLeft()
    {
        return _Bullet_Left;
    }
    public void SetBulletLeftInCharger(float bullets)
    {
        _Bullet_LeftInCharger += bullets;
    }
    public float GetBulletLeftInCharger()
    {
        return _Bullet_LeftInCharger;
    }
}
