using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyBullet :BaseBullet
{
    public bool grabbable;
    protected MC mc;
    public void Awake()
    {
        mc = GameObject.Find("Player(Clone)").GetComponent<MC_Controler>().MC_Data;
    }
    public override void shoot(Vector2 pos, Inventory inv)
    {
        base.shoot(pos, inv);
        grabbable = false;
        purchaseable = false;
    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
     

}

