using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class RifleBullet : AllyBullet
{

    private Rigidbody2D rigidbody2D;
    private Vector3 mouse;
    private Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mouse = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        speed = BD.Bullet_Speed * Time.deltaTime;
        rigidbody2D = GetComponent<Rigidbody2D>();
        Vector3 direction = mouse - transform.position;
        rigidbody2D.velocity = new Vector2(direction.x, direction.y).normalized * BD.Bullet_Speed;
    }





    public override void shoot(Vector2 pos,Inventory inv)
    {
        base.shoot(pos,inv);
      
        for (int i=0; i<3;i++)
        {
            if (BD.GetBulletLeftInCharger()>0)
            {
                Instantiate(this, pos, transform.rotation);
                blast(pos);

                BD.SetBulletLeftInCharger(-1);
            }
            
        }
        Debug.Log(BD.GetBulletLeftInCharger().ToString());
    }

    IEnumerator blast(Vector2 pos)
    {
        Instantiate(this, pos, transform.rotation);
        yield return new WaitForSeconds(0.0001f);
    }

    // Update is called once per frame
    void Update()
    {
        //canviar a partir de posicion de raton ir a esa direcci�n

    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
       
        if (collision.CompareTag("Enemy") )
        {
            Destroy(this.gameObject);
            doDamage(collision);
        }


        if (collision.CompareTag("Player") && grabbable && purchaseable)
        {
            if (mc.GetMoney() >= cost)
            {
                mc.SetMoney(cost * -1);
                bool a = false;
                foreach (var bullet in mc.MC_Weapons)
                {
                    if (this.gameObject.GetType() == bullet.GetType())
                    {
                        bullet.GetComponent<RifleBullet>().BD.SetBulletLeftInCharger(+5);
                        a = true;

                    }
                }
                if (a!) mc.MC_Weapons.Add(this.gameObject);
                Destroy(this.gameObject);
            }

        }
        else if (collision.CompareTag("Player") && grabbable)
        {
            bool a = false;
            foreach (var bullet in collision.gameObject.GetComponent<MC_Controler>().MC_Data.MC_Weapons)
            {
                if (this.gameObject.GetType() == bullet.GetType())
                {
                    Debug.Log(bullet.GetType());
                    bullet.GetComponent<RifleBullet>().BD.SetBulletLeftInCharger(+5);
                    a = true;
                    break;
                }
            }
            if (!a) mc.MC_Weapons.Add(this.gameObject);
            Destroy(this.gameObject);
        }
    }


}
   
