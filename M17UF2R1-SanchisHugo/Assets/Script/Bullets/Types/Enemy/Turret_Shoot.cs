using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Shoot : BaseBullet
{
   
    [SerializeField] private GameObject player;
    [SerializeField] private MC mc;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        Quaternion rotation = Quaternion.LookRotation(player.transform.position - transform.position, transform.TransformDirection(Vector3.up));
        Vector3 direction = player.transform.position - transform.position;
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(direction.x,direction.y).normalized * BD.Bullet_Speed;
        transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
    }
  

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        if (collision.CompareTag("Player"))
        {
            mc.SetHealth(BD.Bullet_Damage*-1);
            Destroy(this.gameObject);
        }
               
    }
}
