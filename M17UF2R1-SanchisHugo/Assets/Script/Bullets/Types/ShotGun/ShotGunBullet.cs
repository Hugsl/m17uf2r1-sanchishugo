using UnityEngine;

public class ShotGunBullet : AllyBullet
{
    
    private Rigidbody2D rigidbody2D;
    private Vector3 mouse;
    private Camera mainCamera;
    private Vector2 initalPosition;
    private float maxRange;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mouse = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        speed = BD.Bullet_Speed * Time.deltaTime;
        rigidbody2D = GetComponent<Rigidbody2D>();
        Vector3 direction = mouse - transform.position;
        rigidbody2D.velocity = new Vector2(direction.x, direction.y).normalized * BD.Bullet_Speed;
        initalPosition = transform.position;
        maxRange = 3f;
    }


    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= initalPosition.x + maxRange || transform.position.y >= initalPosition.y + maxRange || transform.position.x <= initalPosition.x - maxRange || transform.position.y <= initalPosition.y - maxRange)
        {
            Destroy(this.gameObject);
        }

    }

    public override void shoot(Vector2 pos, Inventory inv)
    {

        base.shoot(pos,inv);
        if (BD.GetBulletLeftInCharger() > 0)
        {
            GameObject objectToSpawn = this.gameObject;
            Instantiate(objectToSpawn, pos, transform.rotation);
            pos = new Vector2(pos.x + 0.5f, pos.y + 0.5f);
            Instantiate(objectToSpawn, pos, transform.rotation);
            pos = new Vector2(pos.x - 0.1f, pos.y - 0.1f);
            Instantiate(objectToSpawn, pos, transform.rotation);
           BD.SetBulletLeftInCharger(-1);
        }
    }


    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        if (collision.CompareTag("Enemy") )
        {
            doDamage(collision);
            Destroy(this.gameObject);
        }

        if (collision.CompareTag("Player") && grabbable && purchaseable)
        {
            if (mc.GetMoney() >= cost)
            {
                mc.SetMoney(cost * -1);
                bool a = false;
                foreach (var bullet in mc.MC_Weapons)
                {
                    if (this.gameObject.GetType() == bullet.GetType())
                    {
                        bullet.GetComponent<ShotGunBullet>().BD.SetBulletLeftInCharger(+5);
                        a = true;

                    }
                }
                if (a!) mc.MC_Weapons.Add(this.gameObject);
                Destroy(this.gameObject);
            }

        }
        else if (collision.CompareTag("Player") && grabbable)
        {
            bool a = false;
            foreach (var bullet in collision.gameObject.GetComponent<MC_Controler>().MC_Data.MC_Weapons)
            {
                if (this.gameObject.GetType() == bullet.GetType())
                {
                    Debug.Log(bullet.GetType());
                    bullet.GetComponent<ShotGunBullet>().BD.SetBulletLeftInCharger(+5);
                    a = true;
                    break;
                }
            }
            if (!a) mc.MC_Weapons.Add(this.gameObject);
            Destroy(this.gameObject);
        }
    
}

}
