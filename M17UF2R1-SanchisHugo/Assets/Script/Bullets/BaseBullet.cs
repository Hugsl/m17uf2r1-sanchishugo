using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseBullet : MonoBehaviour
{
    public Bullet BD;
    protected float speed;
    protected Vector3 shootDirection;
    protected Vector2 definitiveDirection;
    protected float reloadTime;
    protected float chargerCapacity;
    protected float maxCapacity;
    public bool purchaseable;
    public int cost;


    public virtual void shoot(Vector2 pos, Inventory inv)
    {
        
        BD.Bullet_Damage = BD.Bullet_Damage * inv.RDamage_Mod;
        BD.Bullet_Speed = BD.Bullet_Speed * inv.RSpeedBullet_Mod;
        BD.Bullet_ReloadTime = BD.Bullet_ReloadTime * inv.RSpeedReload_Mod;
    }
    public virtual void shoot(Vector2 pos)
    {

    }
   
    public virtual void reload()
    {
        if (BD.GetBulletLeft() > 0)
        {
            if (BD.GetBulletLeftInCharger() > 0)
            {
                float bulletstoreload = BD.Bullet_ChargerCapacity - BD.GetBulletLeftInCharger();

                BD.SetBulletLeftInCharger(bulletstoreload);
                BD.SetBulletLeft(-bulletstoreload);
            }
            else
            {
                if (BD.Bullet_ChargerCapacity <= BD.GetBulletLeft())
                {
                    BD.SetBulletLeftInCharger(BD.Bullet_ChargerCapacity);
                    BD.SetBulletLeft(-BD.Bullet_ChargerCapacity);
                }
                else
                {
                    BD.SetBulletLeftInCharger(BD.GetBulletLeft());
                    BD.SetBulletLeft(-BD.GetBulletLeft());
                }
            }
        }
    }
    protected virtual void doDamage(Collider2D collision)
    {
     
        BaseEnemy enemy = collision.gameObject.GetComponent<BaseEnemy>();
        enemy.reciveDamage(BD.Bullet_Damage);
    }

    protected  virtual void OnTriggerEnter2D(Collider2D collision)
    {
        

        if (collision.CompareTag("Wall")|| collision.CompareTag("Door"))
        {
            Destroy(this.gameObject);
        }
      
    }
}
