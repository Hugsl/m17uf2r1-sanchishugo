using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [SerializeField] private List<Bullet> weapons;
    [SerializeField] private MC Player;
    public int numbRooms;

    private static GameManager _instance;
    public static GameManager Instance
    {
        get { return _instance; }
    }
   

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);


    }
    private void Start()
    {
        _restablishBulletData();
        _restablishPlayerData();
    }

    private void _restablishBulletData()
    {
        foreach (Bullet weapon in weapons)
        {
            weapon.SetBulletLeftInCharger(-weapon.GetBulletLeftInCharger());
            weapon.SetBulletLeft(-weapon.GetBulletLeft());
            weapon.SetBulletLeftInCharger(weapon.Charge);
            weapon.SetBulletLeft(weapon.Bullet_Max);
        }
    }
    private void _restablishPlayerData()
    {
        Player.SetHealth(Player.MC_BaseHealth);
        Player.MC_TotalHealth = Player.MC_BaseHealth;
        Player.MC_MoveSpeed = Player.MC_BaseSpeed;
        Player.MC_StuntimeMelee = Player.MC_BaseStuntimeMelee;
        Player.MC_CooldownMelee = Player.MC_BaseCooldownMelee;
        Player.inv.clearall();

    }

    public void checkEnd(Collider2D collider)
    {   
        if (collider.tag == "Player")
        {
            PlayerPrefs.SetString("scoreText", "You Win");
            PlayerPrefs.SetString("scoreNum", GameObject.Find("Score").GetComponent<Text>().text);
            SceneManager.LoadScene("EndScene");
        }
    }
    public void checkEnd()
    {
        if (Player.GetHealth() <= 0)
        {
            PlayerPrefs.SetString("scoreText", "You Loose");
            PlayerPrefs.SetString("scoreNum", GameObject.Find("Score").GetComponent<Text>().text);
            SceneManager.LoadScene("EndScene");
        }
    }
}
