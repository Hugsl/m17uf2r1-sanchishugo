using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controler : MonoBehaviour
{
    public MC mcData;
    private float _health;
    private float _totalHealth;
    [SerializeField] private int punctuation;
    [SerializeField] private Text punctuationText;
    [SerializeField] private float time;

   

    // Start is called before the first frame update
    void Start()
    { punctuation = 1000;
        time = 1;
        punctuationText = GameObject.Find("Score").GetComponent<Text>();
        punctuationText.text = punctuation.ToString();
        _totalHealth = mcData.MC_TotalHealth;
        ChangeBullets();
    }
    private void OnEnable()
    {
        MC.changeLife += ChangeLife;
        MC.changeRangeWeapon += ChangeBullets;
        MC.changeMoney += ChangeCoin;
    }
    private void OnDisable()
    {
        MC.changeMoney -= ChangeCoin;
        MC.changeLife -= ChangeLife;
        MC.changeRangeWeapon -= ChangeBullets;
    }

 
    public void ChangeLife()
    {
        _health = mcData.GetHealth();
        if (_health >= 0) GetComponentInChildren<Scrollbar>().size = _health / _totalHealth;
        else GetComponentInChildren<Scrollbar>().size = 0;
    }

    public void ChangeCoin()
    {
        var _coinText = GameObject.Find("MoneyImage").GetComponentInChildren<Text>();
        _coinText.text = mcData.GetMoney().ToString() + " BT";
        
    }
    public void ChangeBullets()
    {
     
           
       BaseBullet _bulletBase =  mcData.MC_Weapons[mcData.selectionRangedWeapon].GetComponent<BaseBullet>();
        var _bullet = _bulletBase.BD;

        var _bulletLeftInChargerText = GameObject.Find("BulletsInCharger").GetComponent<Text>();
        _bulletLeftInChargerText.text = _bullet.GetBulletLeftInCharger().ToString();
       
        var _bulletLeftText = GameObject.Find("BulletsInStorage").GetComponent<Text>();
        _bulletLeftText.text = _bullet.GetBulletLeft().ToString();
    }
    private void FixedUpdate()
    {
        changePunctuationText();
    }

    void Update()
    {
        ChangeCoin();
    }

    private void changePunctuationText()
    {
        if (time > 0) time-=Time.fixedDeltaTime;
        else {
            setPunctuation(-1);
            time = 1;
        }
    }    
    public void setPunctuation(int a)
    {
        punctuationText = GameObject.Find("Score").GetComponent<Text>();
        punctuation =punctuation + a;
        punctuationText.text = punctuation.ToString();
    }

}

