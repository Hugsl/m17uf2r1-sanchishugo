using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class StartWins : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (File.Exists("scoreSR.txt")) {

            string scoretext = File.ReadAllText("scoreSR.txt");
            var score = JsonUtility.FromJson<Score>(scoretext);
            this.gameObject.GetComponent<Text>().text =$"Consecutive Wins : {score.wins}";
        }
        else this.gameObject.GetComponent<Text>().text = "Try to play!";
    }

 
}
