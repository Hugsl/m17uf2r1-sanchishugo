using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeWinText : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Text>().text = PlayerPrefs.GetString("scoreText");
        GameObject.Find("scoretext").GetComponent<Text>().text = $"Final score: {PlayerPrefs.GetString("scoreNum")}";
    }

}
