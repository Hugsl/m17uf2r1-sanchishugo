using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KeyBehaviour : MonoBehaviour
{
    [SerializeField] private Key key;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            key.TriggerEvent();
            Destroy(this.gameObject);
        }

    }
}
