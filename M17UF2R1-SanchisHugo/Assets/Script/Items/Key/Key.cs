using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Key_Data", menuName = "New Asset/New Key")]
public class Key : ScriptableObject
{
    public int keyNum;

    public static event Action destroyDoors = delegate { };
   

       
    
    public void TriggerEvent()
    {
        destroyDoors.Invoke();
    }
}
