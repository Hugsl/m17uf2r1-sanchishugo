using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private AudioSource audio;
    private Rigidbody2D rb;
    private void LateUpdate()
    {
        Quaternion.Euler(0,0,0);
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        audio = this.gameObject.GetComponent<AudioSource>();
        if (collision.collider.name == "Player")
        {
            var mC_Controler = collision.collider.gameObject.GetComponent<MC_Controler>();
            var player = mC_Controler.MC_Data;
            if (player.GetMoney() < player.MC_MaxMoneyCapacity)
            {

                player.SetMoney(1);
                audio.Play();
                StartCoroutine(AudioPlay());
                Destroy(this.gameObject.GetComponent<Collider2D>());


            }
            else
            {
                
            }
        }
    }
   IEnumerator AudioPlay() {
        yield return new WaitForSeconds(audio.clip.length);
        Destroy(this.gameObject);
    }

}
