using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRestorer : Item
{
    [SerializeField] private float healthHealerMultiplyer;
    
    void Start()
    {
        healthHealerMultiplyer = 0.15f;
        cost = 3;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && purchaseable && mc.GetMoney() >= cost)
        {
            mc.SetMoney(cost * -1);
            mc.SetHealth(mc.MC_TotalHealth *healthHealerMultiplyer);
            Destroy(this.gameObject);

        }
        else if (collision.tag == "Player" && !purchaseable)
        {
            mc.SetHealth(mc.MC_TotalHealth * healthHealerMultiplyer);
            Destroy(this.gameObject);
        }
    }
}
