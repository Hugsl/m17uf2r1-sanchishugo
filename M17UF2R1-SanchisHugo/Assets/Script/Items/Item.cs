using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Item : MonoBehaviour
{
    protected MC mc;
    public bool purchaseable;
    public float cost;
    private void Awake()
    {
        var a = GameObject.Find("Player").GetComponent<MC_Controler>();
        mc = a.MC_Data;
    }

}
