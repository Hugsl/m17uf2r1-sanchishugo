using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DoorX_Data", menuName = "New Asset/New Door")]
public class Door : ScriptableObject
{
    public static event Action RespawnDoors = delegate { };
    [SerializeField] public Key key;
    [SerializeField] public int keyNumCheck;
    public void TriggerEvent()
    {
        Debug.Log("Almost there");
        RespawnDoors.Invoke();
    }
}
