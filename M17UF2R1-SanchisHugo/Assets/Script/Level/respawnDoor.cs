using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class respawnDoor : MonoBehaviour
{
    [SerializeField] private Door door;
    [SerializeField] private List<Enemy> enemies;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            Debug.Log("I've colisioned");
            Destroy(this.gameObject);
            door.TriggerEvent();
            foreach ( Enemy enemy in enemies)
            {
                enemy.TriggerEvent();
            }
            
        }

    }

}
