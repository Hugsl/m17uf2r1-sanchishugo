using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehaviour : MonoBehaviour
{

   [SerializeField] private Door door;
    

    protected void OnEnable()
    {   Door.RespawnDoors -= respawnDoors;
        Key.destroyDoors += destroyDoors;
        Door.RespawnDoors += respawnDoors;
    }





    protected void OnDisable()
    {
        Key.destroyDoors -= destroyDoors;

    }
    public virtual void respawnDoors()
    {
        Debug.Log("I'm in");
        this.gameObject.SetActive(true);
    }


    protected virtual void destroyDoors()
    {
        Debug.Log("Triggered");
        
            Debug.Log("Destroyed");
            this.gameObject.SetActive(false);
        
    }

}
