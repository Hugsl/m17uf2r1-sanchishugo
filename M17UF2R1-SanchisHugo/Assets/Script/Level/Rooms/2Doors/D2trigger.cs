using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D2trigger : MonoBehaviour
{
    public static event Action spawnEnemies = delegate { };

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            spawnEnemies.Invoke();
            this.gameObject.SetActive(false);
        }
    }
}
