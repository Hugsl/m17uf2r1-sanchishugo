using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{

    public List<Item> possibleItems;
    private GameObject PowerUp1;
    private GameObject PowerUp2;
    private GameObject PowerUp3;    
    private GameObject PowerUp4;
    private int size;
    
    private void Start()
    {
        PowerUp1 = GameObject.Find("PowerUp (1)");
        PowerUp2 = GameObject.Find("PowerUp (2)");
        PowerUp3 = GameObject.Find("PowerUp (3)");
        PowerUp4 = GameObject.Find("PowerUp (4)");
        substitute();
        Destroy(PowerUp1);
        Destroy(PowerUp2);
        Destroy(PowerUp3);
        Destroy(PowerUp4);


    }

    private void substitute() {
       
        var a = Instantiate(possibleItems[Random.Range(0,possibleItems.Count)],PowerUp1.transform.position,Quaternion.identity);
        a.purchaseable = true;
        var b = Instantiate(possibleItems[Random.Range(0,possibleItems.Count)],PowerUp2.transform.position,Quaternion.identity);
        b.purchaseable = true;
        var c = Instantiate(possibleItems[Random.Range(0, possibleItems.Count)], PowerUp3.transform.position, Quaternion.identity);
        c.purchaseable = true;
        var d = Instantiate(possibleItems[Random.Range(0, possibleItems.Count)], PowerUp4.transform.position, Quaternion.identity);
        d.purchaseable = true;

    }
}
