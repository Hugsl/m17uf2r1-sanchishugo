using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://youtu.be/hk6cUanSfXQ Video de com generar a partir de prefabs
public class LevelGenerator : MonoBehaviour
{
    public Transform[] startingPositions;
    public GameObject[] rooms;


    [SerializeField] private float moveAmount;
    [SerializeField] private int direction;
    void Awake()
    {
        int randomStartingPosition = Random.Range(0, startingPositions.Length);
        transform.position = startingPositions[randomStartingPosition].position;
        Instantiate(rooms[0], transform.position, Quaternion.identity);
        
    }
    private void Start()
    {
        int a = GameObject.Find("GameManager").GetComponent<GameManager>().numbRooms;
        for (var i =0; i <a ; i++)
        {
            direction = Random.Range(1, 4);
            move();

        }
    }


    private Vector3 startDirection()
    {
        

        switch (direction)
        {
            case 1:
                return new Vector3(0, 0, 0);
                
            case 2:
                return new Vector3(0, 0, 90);
               
            case 3:
                return new Vector3(0, 0, 180);
                
            case 4:
                return new Vector3(0, 0, -90);
                            
            default:
                return new Vector3(0, 0, 0);
        }
    }
    
    private void move()
    {
        Vector2 newPos = new Vector2(0,0);
        if (direction ==1 ) //move right
        {
             newPos = new Vector2(transform.position.x+moveAmount, transform.position.y);
            transform.position = newPos;
        }
        else if (direction == 2) //move up
        {
             newPos = new Vector2(transform.position.x , transform.position.y + moveAmount);
            transform.position = newPos;
        }
       
        else if (direction == 3) // move left
        {
             newPos = new Vector2(transform.position.x - moveAmount, transform.position.y);
            transform.position = newPos;
        }
        else if (direction == 4) // move down
        {
             newPos = new Vector2(transform.position.x , transform.position.y - moveAmount);
            transform.position = newPos;
        }
        Instantiate(rooms[4],transform.position,Quaternion.identity);
        direction = Random.Range(1, 1);
    }


}
