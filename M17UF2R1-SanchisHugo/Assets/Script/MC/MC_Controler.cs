 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MC_Controler : Character
{

    public int numberWeapon;
    public MC MC_Data;
    public AudioClip damagesound;
    // Start is called before the first frame update
    private void Awake()
    {
        MC.changeLife += damageSound;
    }
    void Start()
    {
        GameObject.Find("CM vcam1").GetComponent<CameraBehabiour>().SearchCamera();
        numberWeapon = 0;
        MC_Data.setNumWeapon(numberWeapon);
        hp = MC_Data.GetHealth();
        
        moveSpeed = MC_Data.MC_MoveSpeed * Time.deltaTime;
        rigidBody2D = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        rotateSprite();
        moveCharacter();
        changeWeapon();
    }

    void Update()
    {
        
        if (Input.GetKeyDown("r"))
        {
            reloadBullets();
            MC_Data.setNumWeapon(numberWeapon);
        }
        if (Input.GetMouseButtonDown(0))
        {
            rangeAtack();
            MC_Data.setNumWeapon(numberWeapon);
        }
        if (Input.GetMouseButtonDown(1))
        {
            meleeAtack();
        }
    }
    private void OnDisable()
    {
        MC_Data.SetHealth(MC_Data.MC_TotalHealth);
    }


    protected override void moveCharacter()
    {
        transform.position = new Vector2(moveSpeed*Input.GetAxisRaw("Horizontal")+transform.position.x, moveSpeed * Input.GetAxisRaw("Vertical") + transform.position.y);
       // rigidBody2D.AddForce(new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, Input.GetAxisRaw("Vertical") * moveSpeed));
    }

    protected override void rotateSprite()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5.23f;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x, transform.position.y, transform.position.z));
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;

        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
    protected override void rangeAtack()
    {
        
        BaseBullet bulletData = weapons[numberWeapon].GetComponent<BaseBullet>();
      
        if(bulletData.BD.GetBulletLeftInCharger()>0)  bulletData.shoot(transform.position, MC_Data.inv);
        
    }

    
    protected virtual void reloadBullets()
    {
        BaseBullet bulletData = weapons[numberWeapon].GetComponent<BaseBullet>();
        bulletData.reload();
    }

    protected virtual void changeWeapon()
    {
        checkList();
        var maxWeapons = MC_Data.MC_Weapons.Count-1;
            if (Input.GetAxisRaw("Mouse ScrollWheel")> 0)
            {
                     if (numberWeapon >= maxWeapons)
                        {
                         numberWeapon = 0;
                      }
                    else { numberWeapon = (numberWeapon + 1); }
            MC_Data.setNumWeapon(numberWeapon);

        }

            if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
            {
                if (numberWeapon <= 0)
                {
                numberWeapon = maxWeapons;
                }
                 else { numberWeapon = (numberWeapon - 1); }
            MC_Data.setNumWeapon(numberWeapon);
            //
        }
        
    }


    private void checkList() {
        weapons.Clear();
        foreach (var weapon in MC_Data.MC_Weapons)  weapons.Add(weapon);
    }

    protected override void meleeAtack()
    {
        Debug.Log("MELEE ATACK!");
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Item")
        {
            MC_Data.MC_TotalHealth *= MC_Data.inv.Health_Mod;
            MC_Data.MC_MoveSpeed*=MC_Data.inv.MoveSpeed_Mod;
            MC_Data.MC_MaxMoneyCapacity*=MC_Data.inv.MoneyCarry_Mod;

        }
    }
    protected void damageSound()
    {
        this.gameObject.GetComponent<AudioSource>().Play();
    }
}
