using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


[CreateAssetMenu(fileName ="MC_Data",menuName = "New Asset/New MC")]
public class MC : ScriptableObject
{
    
    //HP Status
    [SerializeField]private float MC_Health;
    public float MC_TotalHealth;
    
    //movespeed
    public float MC_MoveSpeed;


    // Inventory
    public Inventory inv;


    //weapons
    
    public int selectionRangedWeapon;
    public float MC_StuntimeMelee;
    public float MC_CooldownMelee;
    public List<GameObject> MC_Weapons;

    //Money
    [SerializeField] private float MC_actualMoney;
    public float MC_MaxMoneyCapacity;


    //Base stats
    public float MC_BaseHealth;
    public float MC_BaseSpeed;
    public float MC_BaseStuntimeMelee;
    public float MC_BaseCooldownMelee;
    public float MC_BaseMoney;


    //Events
    public static event Action changeLife = delegate { };
    public static event Action changeMoney = delegate { };
    public static event Action changeRangeWeapon = delegate { };
    public static event Action changeStats = delegate { };

    //Functions
    public void SetHealth(float life)
    {
        if (MC_Health+life > MC_TotalHealth)
        {
            MC_Health = MC_TotalHealth;

            changeLife.Invoke();
        }
        else { 
            MC_Health += life;
            changeLife.Invoke();
        }
        GameObject.Find("GameManager").GetComponent<GameManager>().checkEnd();
    }
    public float GetHealth()
    {
        return MC_Health;
    }

    //Weapons Selector (To Change numbers in the ui)

    public void setNumWeapon(int num)
    {
       
        selectionRangedWeapon = num;
        changeRangeWeapon.Invoke();
    }

    //Money,Petrodolars,PetaBytes... idk money 

    public void SetMoney(float value)
    {
        MC_actualMoney += value;
        changeMoney.Invoke();
    }
    public float GetMoney()
    {
        return MC_actualMoney;
    }


}
