using System;
using System.Collections.Generic;
using UnityEngine;


/*
 
 This "inventory" is used to track the modifyers of a player while he is playing

 The modifyers are multiplayers for some of the stats of the player and/or weapons
 
 */



[CreateAssetMenu(fileName = "Inventory_Data", menuName = "New Asset/New Inventory")]
public class Inventory : ScriptableObject
{

    //Reference to the prefab

    public GameObject prefab;

    //List of items by order of recolection

    public List<GameObject> ItemsCatched;

    //Modifyers

    //List of floats to modify Character Values.This is the order (Total Health, MoveSpeed, Money you can carry)
    public float Health_Mod;
    public float MoveSpeed_Mod;
    public float MoneyCarry_Mod;

    // List of floats to modify Meele Weapon Values. This is the order (Stun Time, Range , Cooldown)
    public float MStuntime_Mod;
    public float MRange_Mod;
    public float MCooldown_Mod;



    // List of floats to modify Ranged Weapons Values. This is the order (Damage, Speed of the bullet, Speed of reload)
    public float RDamage_Mod;
    public float RSpeedBullet_Mod;
    public float RSpeedReload_Mod;


    //Events
    public static event Action changeModifyers = delegate { };

    public void clearall(){
        ItemsCatched.Clear();
        Health_Mod = 1;
        MoveSpeed_Mod = 1;
        MoneyCarry_Mod = 1;
        MStuntime_Mod = 1;
        MRange_Mod = 1;
        MCooldown_Mod = 1;
        RDamage_Mod = 1;
        RSpeedBullet_Mod = 1;
        RSpeedReload_Mod = 1;
    }

}
