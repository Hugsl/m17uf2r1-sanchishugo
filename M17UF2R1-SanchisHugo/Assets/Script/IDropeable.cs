using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDropeable
{
    
       void prepareDrop() { } 
   void dropItem(GameObject item) { }
}
