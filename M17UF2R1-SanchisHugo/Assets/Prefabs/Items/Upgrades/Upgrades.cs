using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Upgrades : Item
{
    public Inventory inventory;
  
  

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && purchaseable && mc.GetMoney()>= cost)
        {
            mc.SetMoney(cost*-1);
            modStats();
            Destroy(this.gameObject);

        }
        else if (collision.tag == "Player" && !purchaseable)
        {
            modStats();
            Destroy(this.gameObject);
        }
    }

    private void modStats()
    {


        //add to the list
        mc.inv.ItemsCatched.Add(inventory.prefab);

        //player base stats
        mc.inv.Health_Mod = mc.inv.Health_Mod * inventory.Health_Mod;
        mc.inv.MoveSpeed_Mod = mc.inv.MoveSpeed_Mod * inventory.MoveSpeed_Mod;
        mc.inv.MoneyCarry_Mod = mc.inv.MoneyCarry_Mod*inventory.MoneyCarry_Mod;


        //Meele Weapon stats
        mc.inv.MCooldown_Mod = mc.inv.MCooldown_Mod * inventory.MCooldown_Mod;
        mc.inv.MRange_Mod = mc.inv.MRange_Mod * inventory.MRange_Mod;
        mc.inv.MStuntime_Mod = mc.inv.MStuntime_Mod * inventory.MStuntime_Mod;

        //Ranged weapon stats
        mc.inv.RDamage_Mod = mc.inv.RDamage_Mod * inventory.RDamage_Mod;
        mc.inv.RSpeedBullet_Mod = mc.inv.RSpeedBullet_Mod * inventory.RSpeedBullet_Mod;
        mc.inv.RSpeedReload_Mod = mc.inv.RSpeedReload_Mod * inventory.RSpeedReload_Mod;
    }
}
